# TiddlyWiki Container Image

This image is a simple overlay to easily set up TiddlyWiki on a home machine or server.  There wasn't a TiddlyWiki docker image that I liked for my TrueNAS server so I built my own.

I may add versioning at some point.  For now, just pull on every start to get any updates to the system.

### Container Environment

| Variable Name | Default Value | Notes                                                         |
| ------------- | ------------- | ------------------------------------------------------------- |
| WIKIPORT      | 9042          | Choose whatever you like above 1000 for no potential problems |

There really isn't much environment configuration to be done since I didn't see any reason for it.  You can set the port if you want for the container.  With container port mapping this probalby isn't necessary for any reason, but hey, it's here.

### Container Volumes
| Container Path | Ideal Situation                                                      |
| -------------- | -------------------------------------------------------------------- |
| /var/wiki      | Mount a filesystem/server directory that will be backed-up regularly |
| /etc/wiki      | Mount a read-only directory that contains users.csv and openssl.conf |

TiddlyWiki server keeps files in a single folder, which is `/var/wiki` within the container.  It will maintain these files though edits to the web page.  I would recommend mounting a backed-up system volume / dataset to this location since the first rule of TiddlyWiki is Backup!

Since editing is nice and allowing anyone to edit is problematic, there is also the option to mount a users.csv file to the container for TiddlyWiki's built-in HTTP basic user authentication.  Without this file, the wiki is read-only.  This can be useful if you want to serve a static site.

### TLS Configuration
The `tiddlywiki.sh` script will look for an OpenSSL configuration file at `/etc/wiki/openssl.conf`.  This file should follow the following format to have a somewhat personal distingushed name.

```
[ req ]
prompt = no
x509_extensions = v3_ext
distinguished_name = req_dn
req_extensions = req_ext

[ req_dn ]
C = Country (US, CA, etc)
ST = State or Province
L = Locality (City)
O = Comapny Name / NA
OU = Organizational Unit / NA
emailAddress = admin@yourdomain.com
CN = A descriptive (unique) name for your TiddlyWiki server

[ req_ext ]
subjectAltName = DNS:wiki.yourdomain.com

[ v3_ext ]
basicConstraints = CA:true
```

### Future Enhancements
* [x] SSL so passwords aren't plaintext (doesn't matter too much for LANs, but still)
* [ ] Run as non-root
* [ ] Container image versioning