#!/bin/bash
set -e

WIKIPATH="/var/wiki"
WIKICONFIG="/etc/wiki"
WIKIUSERSCONFIG="/etc/wiki/users.csv"
WIKISSLCONFIG="/etc/wiki/openssl.conf"

# Initialize a local wiki directory if a volume at /etc/wiki was not configured.  If it does exist,
# you'll need to run this command locally to initialize your TiddlyWiki.  TiddlyWiki init does not
# initialize an existing directory.
#
# The tiddlywiki --init command will not do anything if the directory exists even without this check.
if [ ! -d $WIKIPATH ]; then
    echo "Initializing wiki directory $WIKIPATH"
    tiddlywiki $WIKIPATH --init server
fi

# If a credentials file exists we allow writing.  Otherwise, the wiki is read-only or you can
# save locally.
if [ -f $WIKIUSERSCONFIG ]; then
    echo "Using users file $WIKIUSERSCONFIG (Read/Write)"
    WIKICREDENTIALS="credentials=../..$WIKIUSERSCONFIG readers=(anon) writers=(authenticated)"
else
    echo "Default users server configuration (Read Only)"
    WIKICREDENTIALS="readers=(anon)"
fi

# Configure TLS if we have an openssl configuration file.
if [ -f $WIKISSLCONFIG ]; then
    echo "Creating SSL Certs"
    openssl req -new -x509 -nodes -newkey rsa:4096 -keyout server.key \
        -sha256 -days 365 -config $WIKISSLCONFIG -out server.csr
    WIKICERTS="tls-key=../../tiddlywiki/server.key tls-cert=../../tiddlywiki/server.csr"
fi

echo "Starting TiddlyWiki for $WIKIPATH"
tiddlywiki $WIKIPATH --listen \
    port=$WIKIPORT \
    host=0.0.0.0 \
    $WIKICREDENTIALS $WIKICERTS